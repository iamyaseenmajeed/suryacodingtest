//
//  TableViewController.swift
//  CodingTest
//
//  Created by Yaseen Majeed on 02/03/19.
//  Copyright © 2019 Yaseen Majeed. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class TableViewController: UIViewController{
    @IBOutlet weak var tableView: UITableView!
    var tableData = [items]()
    var loadDataAgain = false
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        print("the saved JSON is \(getJSON(appStrings.cachedResult))")
        loadData { (success,loadDataAgain) in
            if loadDataAgain{
               self.reloadData()
            }
        }
        let nib = UINib.init(nibName: "UITaTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "UITaTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self

        
    }
    
    func loadData(complete: @escaping (_ complete: Bool, _ loadDataAgain: Bool) -> Void){
        if tableData.count == 0 {
            parseJSON(getJSON(appStrings.cachedResult)) { (success, data) in
                if success{
                    self.tableData = data
                }
            }
            complete(true,true)
        }
        else{
            complete(true,false)
        }
    }
    
    func reloadData(){
        let parameters = [
            "emailId" : defaults.string(forKey: appStrings.userEmail) ?? "xyz@test.com"
        ]
        APIManager.sharedInstance.PerformApiRequest(.post, path:APIUrls.baseURL.appending("list") , parameters: parameters, headers: nil) { (success, response, jsonResult) in
            if success{
                if let rawJson = jsonResult{
                    saveJSON(json: rawJson, key: appStrings.cachedResult)
                }
                parseJSON(jsonResult){ (success,data) in
                    if success{
                        self.tableData = data
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
}

extension TableViewController:  UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITaTableViewCell", for: indexPath) as! UITaTableViewCell
        cell.tag = indexPath.row
        cell.email.text = tableData[indexPath.row].emailId
        cell.fistName.text = tableData[indexPath.row].firstName
        cell.lastName.text = tableData[indexPath.row].lastName
        //FIXME: Making lazy load of functions currenlty submitting as it is already late
        DispatchQueue.main.async {
            Alamofire.request(self.tableData[indexPath.row].imageURL).response { response in
            if let data = response.data {
                if cell.tag == indexPath.row{
                if let image = UIImage(data: data){
                    cell.cellImage.image = image
                }
              }
            } else {
                print("Data is nil. I don't know what to do :(")
            }

        }
      }
        return cell
    }
    
    
}

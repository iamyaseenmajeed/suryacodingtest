//
//  ViewController.swift
//  CodingTest
//
//  Created by Yaseen Majeed on 02/03/19.
//  Copyright © 2019 Yaseen Majeed. All rights reserved.
//

import UIKit

class EmailViewController: UIViewController {

    @IBOutlet weak var emailInput: UITextField!
    var dataArray = [items]()
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }

    //MARK: Get Data button Action
    @IBAction func getDataClicked(_ sender: Any) {
        if let emailString = emailInput.text,emailString != "" {
            if validateEmail(email: emailString){
            defaults.set(emailString, forKey: appStrings.userEmail)
            let parameters = [
                            "emailId" : emailString
                            ]
            APIManager.sharedInstance.PerformApiRequest(.post, path:APIUrls.baseURL.appending("list") , parameters: parameters, headers: nil) { (success, response, jsonResult) in
                if success{
                    if let rawJson = jsonResult{
                        saveJSON(json: rawJson, key: appStrings.cachedResult)
                    }
                    parseJSON(jsonResult){ (success,data) in
                        if success{
                             self.dataArray = data
                            self.defaults.set(success, forKey: appStrings.isEmailEntered)
                            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "itemList") as? TableViewController{
                                viewController.tableData = self.dataArray
                                self.present(viewController, animated: true, completion: nil)
                            }
                        }
                    }
                }
                
            }
           }
            else{
                UIAlertController.showOneButtonAlert(fromVC: self, title: "Invalid Email", message: "The Email entered is not Valid", successTitle: "OK") {
                    
                }
            }
        }else{
            UIAlertController.showOneButtonAlert(fromVC: self, title: "Empty Email", message: "Email can't be Empty", successTitle: "OK") {
                
            }
        }
        
    }
    
}


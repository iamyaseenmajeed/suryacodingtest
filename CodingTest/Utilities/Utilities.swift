//
//  Utilities.swift
//  CodingTest
//
//  Created by Yaseen Majeed on 03/03/19.
//  Copyright © 2019 Yaseen Majeed. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

let defaults = UserDefaults.standard

//Validating Email string
func validateEmail(email:String) -> Bool {
    
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: email)
    
}

//Saving Json locally in userdefaults
func saveJSON(json: JSON, key:String){
    let jsonString = json.rawString()!
    defaults.setValue(jsonString, forKey: key)
    defaults.synchronize()
}

//Fetching Json from the user Defaults
func getJSON(_ key: String)->JSON {
    var p = ""
    if let buildNumber = defaults.value(forKey: key) as? String {
        p = buildNumber
    }else {
        p = ""
    }
    if  p != "" {
        if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            return try! JSON(data: json)
        } else {
            return JSON("nil")
        }
    } else {
        return JSON("nil")
    }
}

//Parsing JSON returned from API
func parseJSON(_ jsonResult: JSON?,complete: @escaping (_ success: Bool , _ data: [items]) -> Void){
    var dataArray = [items]()
    if let jsonData = jsonResult?["items"].arrayValue{
        for currentElement in jsonData{
            let item = items(firstName: currentElement["firstName"].stringValue, lastName: currentElement["lastName"].stringValue, emailId: currentElement["emailId"].stringValue, url: currentElement["imageUrl"].stringValue)
            dataArray.append(item)
        }
    }
    if dataArray.count != 0{
        complete(true,dataArray)
    }
    else{
        complete(false,dataArray)
    }
}

//Custom Alert function
extension UIAlertController{
    class func showOneButtonAlert(fromVC:UIViewController,title:String,message:String,successTitle:String, onSuccess:(()->())?){
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: successTitle, style: .default) { (_) -> Void in
            onSuccess?()
        }
        alertController.addAction(settingsAction)
        DispatchQueue.main.async {
            fromVC.present(alertController, animated: true, completion: nil)
        }
    }
}

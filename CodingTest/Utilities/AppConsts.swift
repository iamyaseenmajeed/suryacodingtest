//
//  AppConsts.swift
//  CodingTest
//
//  Created by Yaseen Majeed on 03/03/19.
//  Copyright © 2019 Yaseen Majeed. All rights reserved.
//

import Foundation

struct appStrings {
    static let isEmailEntered         :String = "isEmailEntered"
    static let cachedResult           :String = "cachedResult"
    static let userEmail              :String = "userEmail" 
}

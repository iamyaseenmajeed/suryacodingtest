//
//  API.swift
//  CodingTest
//
//  Created by Yaseen Majeed on 03/03/19.
//  Copyright © 2019 Yaseen Majeed. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct APIUrls {
    static let baseURL = "http://surya-interview.appspot.com/"
}


class APIManager {
    let defaults = UserDefaults.standard
    static let sharedInstance = APIManager()
    
    static let sessionManager:SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10 // seconds
        configuration.timeoutIntervalForResource = 10
        return Alamofire.SessionManager.init(configuration: configuration)
    }()
    
    init() {}
    func PerformApiRequest(_ requestMethod: HTTPMethod,
                  path: String,
                  parameters: [String: String]?,
                  headers: [String: String]?,
                  complete: @escaping (_ success: Bool, _ response: HTTPURLResponse?, _ result: JSON?) -> Void){
        APIManager.sessionManager.request(
            path,
            method: requestMethod,
            parameters: parameters,
            encoding: (requestMethod == .post) ? JSONEncoding.default : URLEncoding.default,
            headers: headers).responseJSON { response in
                var success = false
                
                guard let value = response.result.value else {
                    complete(success, response.response, nil)
                    return
                }
                guard let statusCode = response.response?.statusCode else {
                    complete(success, response.response, nil)
                    return
                }
                switch (statusCode) {
                case 200..<299:
                    success = true
                    break
                case 400..<599:
                    success = false
                    // Error
                    break
                default: break
                }
                let jsonValue = JSON(value)
                complete(success, response.response, jsonValue)
                return
                
        }
    }
}

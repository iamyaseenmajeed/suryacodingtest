//
//  UITaTableViewCell.swift
//  CodingTest
//
//  Created by Yaseen Majeed on 02/03/19.
//  Copyright © 2019 Yaseen Majeed. All rights reserved.
//

import UIKit

class UITaTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var fistName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var email: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

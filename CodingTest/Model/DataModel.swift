//
//  DataModel.swift
//  CodingTest
//
//  Created by Yaseen Majeed on 03/03/19.
//  Copyright © 2019 Yaseen Majeed. All rights reserved.
//

import Foundation
import UIKit

class items {
    
    var firstName:String
    var lastName:String
    var emailId:String
    var imageURL:String
    
    init(firstName: String,lastName:String,emailId:String,url:String) {
        self.firstName = firstName
        self.lastName = lastName
        self.emailId = emailId
        self.imageURL = url
    }

}
